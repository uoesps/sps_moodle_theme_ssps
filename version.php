<?php
/*
 * @package   theme_ssps
 * @copyright 2013 University of Edinburgh
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2013050100;
$plugin->requires  = 2013050100;
$plugin->component = 'theme_ssps';
$plugin->dependencies = array(
    'theme_bootstrapbase'  => 2013050100,
);
