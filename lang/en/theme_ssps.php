<?php
/*
 * @package   theme_ssps
 * @copyright 2013 University of Edinburgh
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>SSPS</h2>
<p><img class=img-polaroid src="ssps/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>About</h3>
<p>Based on Clean, which is a modified Moodle bootstrap theme created for Moodle 2.5<br>
</div>
</div>';

$string['configtitle'] = 'SSPS';
$string['footnote'] = 'Footnote';
$string['footnotedesc'] = 'Whatever you add to this textarea will be displayed in the footer throughout your Moodle site.';
$string['pluginname'] = 'SSPS';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';